__Content of The Page:__

### 1. Introduction

### 2. Experiences

### 3. Conclusion

## Introduction

### Myself

<img src="https://gitlab.com/viknes10/introduction-viknes/-/raw/main/File/197279_Picture.jpg" width=150 align=middle>

+ Name = VIKNESWARAN A/L DEVANDRAN

+ Age = 22 Years old

+ Place of Birth = Kuching, Sarawak

+ Univesity = Universiti Putra Malaysia (UPM)

+ Faculty = Engineering

+ Course Taken = Bachelor of Aeropsace Engineering with Honours

### Strenght
1. Running

2. Flights (Aviation)

### Weakness
1. Time Management

2. Eye-sight

## Experiences

### Industrial Training
+ Place of Training : Hornbill Skyways Sdn.Bhd

<img src="https://gitlab.com/viknes10/introduction-viknes/-/raw/main/File/R.jfif" width=190 align=middle>

+ Location : HEAD OFFICE (KUCHING), North Pan Hangar, Kuching International Airport, P.O. Box 1387, 93728 Kuching, Sarawak, Malaysia

+ Duration : 10 Weeks


+ Major Training :-

                 : Continuing Airworthiness Management Organization (CAMO)

                 : CAMO Planning

                 : Technical Records (CAMO)

                 : Technical Services (CAMO)

                 : Production Planning (Part 145)

                 : Aircraft Maintenace (Part 145 - Rotary & Fixed Wing)

                 : Avionics

                 : Engineering Supplies and Store Management

## Conclusion

As a conclusion, i would like to share my experiences so far in the doing the GitLab tasks as been assigned so far to me as to familiarize with the preferences. This is would be a great chances to get use to the GitLab software. Besides, that is all for my sharing here. Thank You.
